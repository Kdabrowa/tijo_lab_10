package pl.edu.pwsztar.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import pl.edu.pwsztar.domain.dto.FigureMoveDto;

@AllArgsConstructor
@Getter
public class Convert {
  private String x;
  private String y;

  private int x_out;
  private int y_out;

  public Convert(FigureMoveDto figureMoveDto){
    String temp = figureMoveDto.getSource();
    this.x= (int)temp.charAt(0)-96;
    this.y = temp.substring(2);
  }

  @Override
  public String toString() {
    return "Convert{" +
        "x=" + x +
        ", y=" + y +
        ", x_out=" + x_out +
        ", y_out=" + y_out +
        '}';
  }
}
