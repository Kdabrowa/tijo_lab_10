package pl.edu.pwsztar.service.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import pl.edu.pwsztar.domain.dto.FigureMoveDto;
import pl.edu.pwsztar.service.ChessService;
import pl.edu.pwsztar.domain.Convert;

@Service
public class ChessServiceImpl implements ChessService {

  @Override
  public boolean isCorrectMove(FigureMoveDto figureMoveDto) {
    Convert convert = new Convert(figureMoveDto);
    System.out.println(convert);
    return true;
  }

}
